/* 
 * This file is part of libcli.
 * Copyright (c) 2021 Eirik Haustveit.
 * 
 * SPDX-License-Identifier: GPL-3.0-only
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBCLI_KEY_CODES_H
#define LIBCLI_KEY_CODES_H

#ifdef __cplusplus
extern "C" {
#endif



#define KEY_CODE_BACKSPACE 0x7f
#define KEY_CODE_BACKSPACE2 0x08
#define KEY_CODE_DELETE 0x7f
#define KEY_CODE_ENTER 0x0a
#define KEY_CODE_LINE_FEED 0x0a
#define KEY_CODE_CARRIAGE_RETURN 0x0d
#define KEY_CODE_ESCAPE 0x1b



#ifdef __cplusplus
}
#endif

#endif