/* 
 * This file is part of libcli.
 * Copyright (c) 2021 Eirik Haustveit.
 * 
 * SPDX-License-Identifier: GPL-3.0-only
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBCLI_H
#define LIBCLI_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>

#include "terminal_key_codes.h"
#include "libcli_queue.h"
#include "libcli_config.h"


/// By default, this macro resolves to the standard assert(). The user can redefine this if necessary.
/// To disable assertion checks completely, make it expand into `(void)(0)`.
#ifndef LIBCLI_ASSERT
#    define LIBCLI_ASSERT(x) assert(x) 
#endif

typedef enum
{
    LIBCLI_OK,
    LIBCLI_AUTH_OK,
    LIBCLI_AUTH_FAILED,
    LIBCLI_AUTH_LOCKED,
    LIBCLI_E_NULL_PTR,
    LIBCLI_E_IO,
    LIBCLI_E_CMD_NOT_FOUND,
    LIBCLI_E_CMD_EMPTY,
    LIBCLI_E_INVALID_ARGS,
    LIBCLI_E_BUF_FULL
} t_libcli_status;

typedef enum _t_libcli_cmd_flags {
    CMD_FLAG_NORMAL = 0,
    CMD_FLAG_INTERACTIVE
} t_libcli_cmd_flags;


typedef struct _t_multibyte_input_key {

    uint8_t input_buf[LIBCLI_MULTICODE_INPUT_MAX_BYTES];
    uint8_t pos;

} t_multibyte_input_key;

typedef t_libcli_status (*t_multibyte_key_handler_ptr)(void*);

typedef struct _t_multibyte_key_cmd {

    uint8_t byte_pattern[LIBCLI_MULTICODE_INPUT_MAX_BYTES];
    uint8_t len;
    t_multibyte_key_handler_ptr handler;

} t_multibyte_key_cmd;

typedef t_libcli_status (*t_cmd_func_ptr)(int ac, char **av);
typedef t_libcli_status (*t_libcli_getc)(char *data_out);
typedef t_libcli_status (*t_libcli_putc)(const char *data_in);

typedef struct _t_libcli_cmd {
    const char *cmd;
    t_cmd_func_ptr fhandle;
    t_libcli_cmd_flags cmd_flags;
} t_libcli_cmd;

typedef struct _t_libcli_ctx {

    t_libcli_cmd *cmd_table;

    char input_buf[LIBCLI_CMD_BUFFER_SIZE];
    uint8_t cursor_pos;

    t_libcli_getc getc;
    t_libcli_putc putc;

    /* Authentication */
    char *cli_password;
    bool cli_locked;
    
    /* Multi byte key codes */
    t_multibyte_input_key mkey;
    t_multibyte_key_cmd *mcmd;

    /* Command arguments */
    uint8_t argc;
    char *argv[LIBCLI_CMD_BUFFER_SIZE];

    /* Command history */
    libcli_queue_ctx_t hqueue;
    char cmd_hist_buf[LIBCLI_CMD_HISTORY_LEN][LIBCLI_CMD_BUFFER_SIZE];
    
} t_libcli_ctx;


t_libcli_status libcli_init(t_libcli_ctx *ctx, t_libcli_cmd *cmd_table, t_libcli_getc getc, t_libcli_putc putc, bool cli_locked, char *cli_password);
t_libcli_status libcli_initial_prompt(t_libcli_ctx *ctx);
t_libcli_status libcli_read(t_libcli_ctx *ctx);

t_libcli_status libcli_conf_authentication(t_libcli_ctx *ctx, bool cli_locked, char *cli_password);
t_libcli_status libcli_lock_terminal(t_libcli_ctx *ctx);

#ifdef __cplusplus
}
#endif

#endif