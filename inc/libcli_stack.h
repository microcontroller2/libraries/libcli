/* 
 * This file is part of libcli.
 * Copyright (c) 2021 Eirik Haustveit.
 * 
 * SPDX-License-Identifier: GPL-3.0-only
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBCLI_STACK_H
#define LIBCLI_STACK_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdlib.h>

#define EMPTY -1
#define LIBCLI_STACK_LEN 32

typedef int libcli_stack_data_t;

typedef struct _libcli_stack_ctx_t{
    
    int8_t top;
    int8_t pos; // Keep track of current position while traversing without poping data
    libcli_stack_data_t stack[LIBCLI_STACK_LEN];
    
} libcli_stack_ctx_t;

typedef enum _libcli_stack_res_t {
    RES_OK = 0,
    RES_ERROR,
    RES_FULL,
    RES_EMPTY,
    RES_BOTTOM,
    RES_TOP
} libcli_stack_res_t;

/** Initialize the stack
  @param   ctx      The stack context
  @return  Error code
  @note This function is intended for internal use by libcli
*/
libcli_stack_res_t libcli_stack_init(libcli_stack_ctx_t *ctx);


/** Push a value on to the stack
  @param   ctx      The stack context
  @param   data_in  The data we are pushing
  @return  Error code
  @note This function is intended for internal use by libcli
*/
libcli_stack_res_t libcli_stack_push(libcli_stack_ctx_t *ctx, libcli_stack_data_t *data_in);


/** Pop a value off the stack
  @param   ctx      The stack context
  @param   data_in  Pointer to storage location for the returned data
  @return  Error code
  @note This function is intended for internal use by libcli
*/
libcli_stack_res_t libcli_stack_pop(libcli_stack_ctx_t *ctx, libcli_stack_data_t *data_out);


/** Traverse down the stack without poping
  @param   ctx      The stack context
  @param   data_in  Pointer to storage location for the returned data
  @return  Error code
  @note This function is intended for internal use by libcli
*/
libcli_stack_res_t libcli_stack_traverse_down(libcli_stack_ctx_t *ctx, libcli_stack_data_t *data_out);

/** Traverse up the stack without poping
  @param   ctx      The stack context
  @param   data_in  Pointer to storage location for the returned data
  @return  Error code
  @note This function is intended for internal use by libcli
*/
libcli_stack_res_t libcli_stack_traverse_up(libcli_stack_ctx_t *ctx, libcli_stack_data_t *data_out);



#ifdef __cplusplus
}
#endif

#endif