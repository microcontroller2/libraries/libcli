/* 
 * This file is part of libcli.
 * Copyright (c) 2021 Eirik Haustveit.
 * 
 * SPDX-License-Identifier: GPL-3.0-only
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBCLI_QUEUE_H
#define LIBCLI_QUEUE_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdlib.h>
#include <assert.h>
#include "libcli_config.h"

//#define LIBCLI_QUEUE_LEN 8

typedef char libcli_queue_data_t;

typedef char cmd_line_buffer_t[LIBCLI_CMD_BUFFER_SIZE];

/* Opaque circular buffer structure */
//typedef struct _libcli_queue_ctx_t libcli_queue_ctx_t;

/* Handle type for the users to interact with the API */
//typedef libcli_queue_ctx_t *libcli_queue_hactx;

typedef enum _libcli_queue_res_t {
    QUEUE_RES_OK = 0,
    QUEUE_RES_FULL,
    QUEUE_RES_EMPTY,
    QUEUE_RES_BOTTOM,
    QUEUE_RES_TOP
} libcli_queue_res_t;


/**
 * Data structure for the queue
*/
typedef struct _libcli_queue_ctx_t {
    /*@{*/ 
    int8_t head; /**< The queue head index */
    int8_t tail; /**< The queue tail index */
    int8_t size; /**< The maximum number of entries in the queue */
    /*@}*/ 
    uint8_t num_entries; // Current number of entries in the queue
    int8_t pos; // Keep track of current position while traversing without removing data
    cmd_line_buffer_t *queue_buffer;

    //libcli_queue_data_t queue_buf; // Pointer to buffer which holds the queue data
    //uint8_t element_size; // Size of each element in the queue
    
} libcli_queue_ctx_t;


//array_of_20_uint8_t *matrix_ptr = l_matrix;

/**
 * @brief Initialize the queue
 * 
 * @param ctx Pointer to the queue context struct which is to be initialized
 * @param queue_buffer Pointer to the buffer to use for the enqueued data
 * @param size Size of the buffer in number of elements (i.e. not the number of bytes)
 * @return libcli_queue_res_t 
 */
libcli_queue_res_t libcli_queue_init(libcli_queue_ctx_t *ctx, cmd_line_buffer_t *queue_buffer, size_t size);

libcli_queue_res_t libcli_queue_free(libcli_queue_ctx_t *ctx);

/**
 * @brief Reset the queue to empty state
 * 
 * @param ctx The queue context
 */
void libcli_queue_reset(libcli_queue_ctx_t *ctx);

/**
 * @brief Check if the queue is empty
 * 
 * @param ctx 
 * @return uint8_t Returns one of the queue is empty, zero otherwize
 */
uint8_t libcli_queue_is_empty(libcli_queue_ctx_t *ctx);

/**
 * @brief Check if the queue is full
 * 
 * @param ctx The queue contest
 * @return uint8_t Returns one of the queue is empty, zero otherwize
 */
uint8_t libcli_queue_is_full(libcli_queue_ctx_t *ctx);

/**
 * @brief Returns the capacity of the queue
 * 
 * @param ctx The queue context
 * @return uint8_t The maximum number of elements that can be added to the queue
 */
uint8_t libcli_queue_capacity(libcli_queue_ctx_t *ctx);

/**
 * @brief Returns the number of entries in the queue
 * 
 * @param ctx The queue context
 * @return uint8_t The number of entries currently existing in the queue
 */
uint8_t libcli_queue_num_entries(libcli_queue_ctx_t *ctx);

/**
 * @brief Add a new element to the queue
 * 
 * @param ctx The queue context
 * @param data_in Pointer to the data to be copied in to the queue internal storage
 * @return libcli_queue_res_t Status code to inform the caller of possible errors
 */
libcli_queue_res_t libcli_queue_enqueue(libcli_queue_ctx_t *ctx, const libcli_queue_data_t *data_in);

/**
 * @brief Add a new element to the queue. Overwrite the oldest element if the queue is full.
 * 
 * @param ctx The queue context
 * @param data_in Pointer to the data to be enqueued
 * @return libcli_queue_res_t Status code to onform the caller of possible errors
 */
libcli_queue_res_t libcli_queue_enqueuef(libcli_queue_ctx_t *ctx, const libcli_queue_data_t *data_in);

/**
 * @brief Remove a element from the queue
 * 
 * @param ctx The queue context
 * @param data_out Pointer to allocated memory where the data which is removed from the queue will be placed
 * @return libcli_queue_res_t Status code to inform the caller of possible errors
 */
libcli_queue_res_t libcli_queue_dequeue(libcli_queue_ctx_t *ctx, libcli_queue_data_t *data_out);

/**
 * @brief Traverse the queue in forward direction without making any change to the queue
 *
 * @details Traversing forwards means looking at the next newest element after the element in the current traverse
 * position. If you are at the oldest element in the queue, traversing forward will move you to the newest element.
 *  
 * @param ctx The queue context
 * @param data_out Pointer to allocated memory where a copy of the data in the current queue location will be placed
 * @return libcli_queue_res_t 
 */
libcli_queue_res_t libcli_queue_traverse_forward(libcli_queue_ctx_t *ctx, libcli_queue_data_t *data_out);


/**
 * @brief Traverse the queue in backward direction without making any change to the queue
 *
 * @details Traversing backwards means going back from the current traverse position. If you are positioned at the
 * newest element in the queue, traversing backwards will move you to the oldes element in the queue.
 *  
 * @param ctx The queue context
 * @param data_out Pointer to allocated memory where a copy of the data in the current queue location will be placed
 * @return libcli_queue_res_t 
 */
libcli_queue_res_t libcli_queue_traverse_backward(libcli_queue_ctx_t *ctx, libcli_queue_data_t *data_out);


#ifdef __cplusplus
}
#endif

#endif