/* 
 * This file is part of libcli.
 * Copyright (c) 2021 Eirik Haustveit.
 * 
 * SPDX-License-Identifier: GPL-3.0-only
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBCLI_CONFIG_H
#define LIBCLI_CONFIG_H

#ifdef __cplusplus
extern "C" {
#endif


#define LIBCLI_CMD_BUFFER_SIZE 32
#define LIBCLI_CMD_HISTORY_LEN 5
#define LIBCLI_MULTICODE_INPUT_MAX_BYTES 5

//#define LIBCLI_PARAM_REQUIRE_SPACE


#ifdef __cplusplus
}
#endif

#endif