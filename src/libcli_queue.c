
#include <string.h>

#include "libcli_queue.h"



//TODO: This approach only allows a single instance of the queue
//static libcli_queue_ctx_t static_libcli_queue_ctx_g;
//static libcli_queue_handle_t libcli_queue_ctx_g = &static_libcli_queue_ctx_g;

libcli_queue_res_t libcli_queue_init(libcli_queue_ctx_t *ctx, cmd_line_buffer_t *queue_buffer, size_t size){

    assert(ctx && queue_buffer && size); // Make sure they are not zero

    //libcli_queue_handle_t ctx = malloc(sizeof(libcli_queue_handle_t)); //TODO: We would like encaptulation, but also not to use malloc()

    //assert(libcli_queue_ctx_g);

    ctx->size = size;
    ctx->queue_buffer = queue_buffer;

    libcli_queue_reset(ctx);

    assert(libcli_queue_is_empty(ctx));

    return QUEUE_RES_OK;
}


libcli_queue_res_t libcli_queue_free(libcli_queue_ctx_t *ctx){

    assert(ctx);
    //free(ctx); // free() does not return anything

    return QUEUE_RES_OK;
}

void libcli_queue_reset(libcli_queue_ctx_t *ctx){

    assert(ctx);

    ctx->num_entries = 0;
    ctx->head = 0;
    ctx->tail = 0;
    ctx->pos = 0;

}

uint8_t libcli_queue_is_empty(libcli_queue_ctx_t *ctx){

    assert(ctx);

    return (ctx->num_entries == 0); // Check if there are no entries
}

uint8_t libcli_queue_is_full(libcli_queue_ctx_t *ctx){

    assert(ctx);

    return (ctx->num_entries == ctx->size); // Check if queue is full
}

uint8_t libcli_queue_capacity(libcli_queue_ctx_t *ctx){

    assert(ctx);

    return ctx->size;
}

uint8_t libcli_queue_num_entries(libcli_queue_ctx_t *ctx){

    assert(ctx);

    return ctx->num_entries;
}

/** Helper function to advance the pointers when new elements are added
 * 
 * We add new elements at the tail of our circular buffer
*/
static void _advance_pointers(libcli_queue_ctx_t *ctx){

    assert(ctx);

    if(ctx->num_entries == ctx->size){
        ctx->head++;
        if(ctx->head == ctx->size){
            ctx->head = 0;
        }
    }
    else{
        ctx->num_entries++;
    }

    /**
     * When adding new stuff to the queue, set pos to the newest element
     * Do this before we advance the tail pointer.
    */
    ctx->pos = ctx->tail; 
    
    ctx->tail++;
    if(ctx->tail == ctx->size){
        ctx->tail = 0;
    }

}

libcli_queue_res_t libcli_queue_enqueue(libcli_queue_ctx_t *ctx, const libcli_queue_data_t *data_in){

    assert(ctx && data_in);

    if(libcli_queue_is_full(ctx)){
        return QUEUE_RES_FULL;
    }


    memset(ctx->queue_buffer[ctx->tail], 0x00, LIBCLI_CMD_BUFFER_SIZE);
    strcpy(ctx->queue_buffer[ctx->tail], data_in);
    //ctx->queue_buf[ctx->tail] = *data_in;
    _advance_pointers(ctx);

    return QUEUE_RES_OK;
}

libcli_queue_res_t libcli_queue_enqueuef(libcli_queue_ctx_t *ctx, const libcli_queue_data_t *data_in){

    assert(ctx && data_in);

    memset(ctx->queue_buffer[ctx->tail], 0x00, LIBCLI_CMD_BUFFER_SIZE);
    strcpy(ctx->queue_buffer[ctx->tail], data_in);
    //ctx->queue_buf[ctx->tail] = *data_in;
    _advance_pointers(ctx);

    return QUEUE_RES_OK;
}

static void _retreat_pointers(libcli_queue_ctx_t *ctx){

    assert(ctx);

    if(ctx->num_entries > 0) // Probably not neccesary to check this, but just to be safe.
        ctx->num_entries--;

    ctx->head++;
    if(ctx->head >= ctx->size){
        ctx->head = 0;
    }
}

libcli_queue_res_t libcli_queue_dequeue(libcli_queue_ctx_t *ctx, libcli_queue_data_t *data_out){

    assert(ctx);

    if(libcli_queue_is_empty(ctx)){
        return QUEUE_RES_EMPTY;
    }

    if(data_out != NULL){ // We support dequeueing without using the data

        strcpy(data_out, ctx->queue_buffer[ctx->head]);
 
        memset(ctx->queue_buffer[ctx->head], 0x00, LIBCLI_CMD_BUFFER_SIZE); //TODO: Do we need to do this?
        
        //*data_out = ctx->queue_buf[ctx->head];
    }
    _retreat_pointers(ctx);

    return QUEUE_RES_OK;
}


libcli_queue_res_t libcli_queue_traverse_forward(libcli_queue_ctx_t *ctx, libcli_queue_data_t *data_out){

    libcli_queue_res_t res = QUEUE_RES_OK;

    assert(ctx);

    if(libcli_queue_is_empty(ctx)){
        return QUEUE_RES_EMPTY;
    }

//    //if(   ){
//        ctx->pos++;
//        if(ctx->pos >= ctx->size){
//            ctx->pos = 0;
//        }
//    //}

    if(data_out != NULL){ // We support traversing without using the data

        strcpy(data_out, ctx->queue_buffer[ctx->pos]);

        //*data_out = ctx->queue_buf[ctx->head];
    }
 
    ctx->pos++;
    if(ctx->pos >= ctx->size){
        ctx->pos = 0;
    }

    return res;
}



libcli_queue_res_t libcli_queue_traverse_backward(libcli_queue_ctx_t *ctx, libcli_queue_data_t *data_out){

    libcli_queue_res_t res = QUEUE_RES_OK;

    assert(ctx);

    if(libcli_queue_is_empty(ctx)){
        return QUEUE_RES_EMPTY;
    }

    if(data_out != NULL){ // We support traversing without using the data

        strcpy(data_out, ctx->queue_buffer[ctx->pos]);

        //*data_out = ctx->queue_buf[ctx->head];
    }

   // if(ctx->pos == ctx->head){
   //     return QUEUE_RES_BOTTOM;
   // }

    if(ctx->pos == 0){
        ctx->pos = ctx->size - 1;
    }
    else{
        ctx->pos--;
    }

//    	if (!ctx->hpos)
//			ctx->hpos = CLI_CMD_HISTORY_LEN - 1;
//		else
//			ctx->hpos--;
//

    return res;
}

