#include <ctype.h>
#include <stdio.h>
#include <string.h>

#include "libcli.h"
#include "terminal_key_codes.h"

const char libcli_prompt[] = "VIBE>";
const char libcli_auth_prompt[] = "PASSWORD:";

static const char help_cmd[] = "help";
static const char lock_cmd[] = "lock";

/**
 * @brief Output a string on the CLI
 * 
 * @param ctx 
 * @param data_in 
 */
static void _libcli_putstr(t_libcli_ctx *ctx, const char *data_in);

/**
 * @brief Key up handle
 * 
 * @param data 
 * @return t_libcli_status 
 */
static t_libcli_status _libcli_key_up(void* data);

/**
 * @brief Key down handle
 * 
 * @param data 
 * @return t_libcli_status 
 */
static t_libcli_status _libcli_key_down(void* data);

/**
 * @brief Key right handle
 * 
 * @param data 
 * @return t_libcli_status 
 */
static t_libcli_status _libcli_key_right(void* data);

/**
 * @brief Key left handle
 * 
 * @param data 
 * @return t_libcli_status 
 */
static t_libcli_status _libcli_key_left(void* data);

/**
 * @brief Function key F2 handle
 * 
 * @param data 
 * @return t_libcli_status 
 */
static t_libcli_status _libcli_key_f2(void* data);

/**
 * @brief Function key F3 handle
 * 
 * @param data 
 * @return t_libcli_status 
 */
static t_libcli_status _libcli_key_f3(void* data);

/**
 * @brief Home key handle
 * 
 * @param data 
 * @return t_libcli_status 
 */
static t_libcli_status _libcli_key_home(void* data);

/**
 * @brief End key handle
 * 
 * @param data 
 * @return t_libcli_status 
 */
static t_libcli_status _libcli_key_end(void* data);

/**
 * @brief Page up key handle
 * 
 * @param data 
 * @return t_libcli_status 
 */
static t_libcli_status _libcli_key_pgup(void* data);

/**
 * @brief Page down key handle
 * 
 * @param data 
 * @return t_libcli_status 
 */
static t_libcli_status _libcli_key_pgdown(void* data);

/**
 * @brief Insert key handle
 * 
 * @param data 
 * @return t_libcli_status 
 */
static t_libcli_status _libcli_key_insert(void* data);

/**
 * @brief Delete key handle
 * 
 * @param data 
 * @return t_libcli_status 
 */
static t_libcli_status _libcli_key_delete(void* data);

/**
 * @brief aelig key handle
 * 
 * @param data 
 * @return t_libcli_status 
 */
static t_libcli_status _libcli_key_aelig(void* data);

/**
 * @brief oslash key handle
 * 
 * @param data 
 * @return t_libcli_status 
 */
static t_libcli_status _libcli_key_oslash(void* data);

/**
 * @brief aring key handle
 * 
 * @param data 
 * @return t_libcli_status 
 */
static t_libcli_status _libcli_key_aring(void* data);

/**
 * @brief Prints a listing of all the supported commands
 * 
 * @param ctx 
 * @return t_libcli_status 
 */
static t_libcli_status _libcli_print_cmd_listing(t_libcli_ctx *ctx);

/**
 * @brief Print the CLI prompt
 * 
 * @param ctx 
 * @param newline 
 */
static void _libcli_prompt(t_libcli_ctx *ctx, uint8_t newline);

/**
 * @brief Print the CLI password prompt
 * 
 * @param ctx 
 * @param newline 
 */
static void _libcli_auth_prompt(t_libcli_ctx *ctx, uint8_t newline);

/**
 * @brief Add a new command to the CLI history
 * 
 * @param ctx 
 * @return t_libcli_status 
 */
static t_libcli_status _libcli_cmd_history_add(t_libcli_ctx *ctx);

/**
 * The supported key codes are dependent upon the terminal you are using
 * The following should work on most modern terminal emulators, if in doubt use picocom.
*/
t_multibyte_key_cmd g_multibyte_key_cmd[] = {
    {
        .byte_pattern = { 0x1b, 0x5b, 0x41 },
        .len = 3,
        .handler = _libcli_key_up
    },
    {
        .byte_pattern = { 0x1b, 0x5b, 0x42 },
        .len = 3,
        .handler = _libcli_key_down
    },
    {
        .byte_pattern = { 0x1b, 0x5b, 0x43 },
        .len = 3,
        .handler = _libcli_key_right
    },
    {
        .byte_pattern = { 0x1b, 0x5b, 0x44 },
        .len = 3,
        .handler = _libcli_key_left
    },
    {
        .byte_pattern = { 0x1b, 0x4f, 0x51 },
        .len = 3,
        .handler = _libcli_key_f2
    },
    {
        .byte_pattern = { 0x1b, 0x4f, 0x52 },
        .len = 3,
        .handler = _libcli_key_f3
    },
    {
        .byte_pattern = { 0x1b, 0x5b, 0x48 },
        .len = 3,
        .handler = _libcli_key_home
    },
    {
        .byte_pattern = { 0x1b, 0x5b, 0x46 },
        .len = 3,
        .handler = _libcli_key_end
    },
    {
        .byte_pattern = { 0x1b, 0x5b, 0x35, 0x7E },
        .len = 4,
        .handler = _libcli_key_pgup
    },
    {
        .byte_pattern = { 0x1b, 0x5b, 0x36, 0x7E},
        .len = 4,
        .handler = _libcli_key_pgdown
    },
    {
        .byte_pattern = { 0x1b, 0x5b, 0x32, 0x7E},
        .len = 4,
        .handler = _libcli_key_insert
    },
    {
        .byte_pattern = { 0x1b, 0x5b, 0x33, 0x7E},
        .len = 4,
        .handler = _libcli_key_delete
    },
    //TODO: Should we ever bother to add support for æøå?
    {
        .byte_pattern = { 0xc3, 0xa6 }, // æ
        .len = 2,
        .handler = _libcli_key_aelig
    },
    {
        .byte_pattern = { 0xc3, 0xb8 }, // ø
        .len = 2,
        .handler = _libcli_key_oslash
    },
    {
        .byte_pattern = { 0xc3, 0xa5 }, // å
        .len = 2,
        .handler = _libcli_key_aring
    },

    // Null termination
    { {0x00}, 0x00, 0x00}
};


static void _libcli_putstr(t_libcli_ctx *ctx, const char *data_in){

   while(*data_in != '\0'){
        ctx->putc(data_in);
        data_in++;
    }

}

t_libcli_status libcli_init(t_libcli_ctx *ctx, t_libcli_cmd *cmd_table, t_libcli_getc getc, t_libcli_putc putc, bool cli_locked, char *cli_password){

    t_libcli_status ret = LIBCLI_OK;
    
    memset(ctx, 0x00, sizeof(t_libcli_ctx));
    ctx->cmd_table = cmd_table;
    ctx->mcmd = g_multibyte_key_cmd;

    ctx->getc = getc;
    ctx->putc = putc;

    libcli_conf_authentication(ctx, cli_locked, cli_password);
    //ctx->cli_locked = cli_locked;
    //ctx->cli_password = cli_password;

    libcli_queue_init(&ctx->hqueue, ctx->cmd_hist_buf, LIBCLI_CMD_HISTORY_LEN);


    return ret;
}

t_libcli_status libcli_conf_authentication(t_libcli_ctx *ctx, bool cli_locked, char *cli_password){

    t_libcli_status ret = LIBCLI_OK;
    
    ctx->cli_locked = cli_locked;
    ctx->cli_password = cli_password;

    return ret;
}

t_libcli_status libcli_lock_terminal(t_libcli_ctx *ctx){

    t_libcli_status ret = LIBCLI_OK;
    
    ctx->cli_locked = true;
    _libcli_putstr(ctx, "CLI locked\r\n");

    return ret;
}


/**
 * Fancy cursor movement is usually not supported on simple uart terminals,
 * but returning to the beginning of the line with '\r' usually is.
 * This allows us to clear the line by painting over the text with spaces
*/
static void _libcli_clear_line(t_libcli_ctx *ctx){

    _libcli_putstr(ctx, "\r");
    // TODO: Strlen prompt 
    for(uint8_t i = 0; i < (LIBCLI_CMD_BUFFER_SIZE + strlen(libcli_prompt)); i++){
        _libcli_putstr(ctx, " ");
    }
    _libcli_putstr(ctx, "\r");
    
}

static void _libcli_prompt(t_libcli_ctx *ctx, uint8_t newline){

    if(newline){
        _libcli_putstr(ctx, "\r\n");
    }
    _libcli_putstr(ctx, libcli_prompt);
}

static void _libcli_auth_prompt(t_libcli_ctx *ctx, uint8_t newline){

    if(newline){
        _libcli_putstr(ctx, "\r\n");
    }
    _libcli_putstr(ctx, libcli_auth_prompt);
}

static t_libcli_status _libcli_authenticate(t_libcli_ctx *ctx){

    t_libcli_status ret = LIBCLI_OK;
    
    if(!strncmp(ctx->cli_password, ctx->input_buf, strlen(ctx->cli_password))){
        ctx->cli_locked = false;
        _libcli_putstr(ctx, "Authentication successful\r\n");
        ret = LIBCLI_AUTH_OK;
    }
    else{
        _libcli_putstr(ctx, "Incorrect password\r\n");
        ret = LIBCLI_AUTH_FAILED;
    }

    return ret;
}

static t_libcli_status _libcli_interpret_cmd(t_libcli_ctx *ctx){

    uint8_t i = 0;
    t_libcli_status ret = LIBCLI_OK;

    if(!strlen(ctx->input_buf)){
        // TODO: If the command line is empty, repeat the previous command (in the style of GDB)
        return LIBCLI_E_CMD_EMPTY;
    }

    _libcli_cmd_history_add(ctx);

    char *the_rest = ctx->input_buf;
    ctx->argc = 0;

    /* Get the first token, which is the command name */
    ctx->argv[ctx->argc] = strtok_r(ctx->input_buf, " ", &the_rest);

    /* Get the command parameters */
    while((ctx->argv[ctx->argc] != NULL) && (ctx->argc < LIBCLI_CMD_BUFFER_SIZE - 1)){
        ctx->argv[++(ctx->argc)] = strtok_r(NULL, " ", &the_rest);
    }

    while(ctx->cmd_table[i].fhandle){

        if(!strncmp(help_cmd, ctx->input_buf, strlen(help_cmd))){
            _libcli_print_cmd_listing(ctx);
            break;
        }
        if(!strncmp(lock_cmd, ctx->input_buf, strlen(lock_cmd))){
            libcli_lock_terminal(ctx);
            ret = LIBCLI_AUTH_LOCKED;
            break;
        }
        // Compare the recive buffer with the supported commands
        #ifdef LIBCLI_PARAM_REQUIRE_SPACE
        // Do not match on words that exceed the command. E.g. helpme should not match help
        // TODO: Fix this
        if(!strncmp(ctx->cmd_table[i].cmd, ctx->argv[0], strlen(ctx->argv[0]))){
        #else
        else if(!strncmp(ctx->cmd_table[i].cmd, ctx->input_buf, strlen(ctx->cmd_table[i].cmd))){
        #endif


            // TODO: Perhaps it would be better that the command handler function is responsible for creating the loop, instead of having it here?
            if(ctx->cmd_table[i].cmd_flags == CMD_FLAG_INTERACTIVE){
                char rx_byte = 0;
                _libcli_putstr(ctx, "Interactive command. Press X to terminate.\r\n");
                while(!((rx_byte == 'X') || (rx_byte == 'x'))){
                    ctx->getc(&rx_byte);
                    ctx->cmd_table[i].fhandle(ctx->argc, ctx->argv);
                }
            }
            else{
                // Call the command handler function
                ctx->cmd_table[i].fhandle(ctx->argc, ctx->argv);
            }

            break;
        }
        i++;
    }

    // If current fhandle is zero, it means that we searched through all the commands without a match
    if(!ctx->cmd_table[i].fhandle){
        ret = LIBCLI_E_CMD_NOT_FOUND;
    }

    return ret;
}

static t_libcli_status _libcli_interpret_cmd_result(t_libcli_ctx *ctx, t_libcli_status res){

    t_libcli_status ret = LIBCLI_OK;

    switch(res){
        case LIBCLI_E_CMD_NOT_FOUND:
            _libcli_putstr(ctx, "Command not found: ");
            _libcli_putstr(ctx, ctx->input_buf);
            break;
        case LIBCLI_E_CMD_EMPTY:
            _libcli_putstr(ctx, "Command empty");
            break;
        default:
            break;
    }

    return ret;
}

static t_libcli_status _libcli_cmd_history_add(t_libcli_ctx *ctx){

    t_libcli_status ret = LIBCLI_OK;
    
    libcli_queue_enqueuef(&(ctx->hqueue), ctx->input_buf);

    return ret;
}

/**
 * Emulate command input by filling the user input buffer and moving the
 * cursor position.
*/
static t_libcli_status _libcli_emulate_cmd_input(t_libcli_ctx *ctx, char *data_in){

    t_libcli_status ret = LIBCLI_OK;

    memset(ctx->input_buf, 0x00, LIBCLI_CMD_BUFFER_SIZE);
    strcpy(ctx->input_buf, data_in);

    ctx->cursor_pos = strlen(ctx->input_buf);

    _libcli_clear_line(ctx);
    _libcli_prompt(ctx, 0);
    _libcli_putstr(ctx, ctx->input_buf);

    return ret;
}

static t_libcli_status _libcli_key_up(void* data){

    t_libcli_status ret = LIBCLI_OK;

    t_libcli_ctx *ctx = (t_libcli_ctx *)data;

    char buffer[LIBCLI_CMD_BUFFER_SIZE];
    if(libcli_queue_traverse_backward(&(ctx->hqueue), buffer) == QUEUE_RES_OK){
        _libcli_emulate_cmd_input(ctx, buffer);
    }

    return ret;
}

static t_libcli_status _libcli_key_down(void* data){

    t_libcli_status ret = LIBCLI_OK;

    t_libcli_ctx *ctx = (t_libcli_ctx *)data;

    char buffer[LIBCLI_CMD_BUFFER_SIZE];
    if(libcli_queue_traverse_forward(&(ctx->hqueue), buffer) == QUEUE_RES_OK){
        _libcli_emulate_cmd_input(ctx, buffer);
    }

    return ret;
}


static t_libcli_status _libcli_key_right(void* data){

    t_libcli_status ret = LIBCLI_OK;

    t_libcli_ctx *ctx = (t_libcli_ctx *)data;
    _libcli_putstr(ctx, "Key right.");

    return ret;
}

static t_libcli_status _libcli_key_left(void* data){

    t_libcli_status ret = LIBCLI_OK;

    t_libcli_ctx *ctx = (t_libcli_ctx *)data;
    //_libcli_putstr(ctx, "Key left.");

    // Left key does the same as backspace
    // TODO: Implement the required functionality to edit the command line
    if(ctx->cursor_pos){
        //ctx->input_buf[ctx->cursor_pos--] = '\0';
        ctx->input_buf[--(ctx->cursor_pos)] = '\0';
        _libcli_putstr(ctx, "\b \b");
    }

    return ret;
}

static t_libcli_status _libcli_key_f2(void* data){

    t_libcli_status ret = LIBCLI_OK;

    t_libcli_ctx *ctx = (t_libcli_ctx *)data;
    _libcli_putstr(ctx, "Key F2.");

    return ret;
}

static t_libcli_status _libcli_key_f3(void* data){

    t_libcli_status ret = LIBCLI_OK;

    t_libcli_ctx *ctx = (t_libcli_ctx *)data;
    _libcli_putstr(ctx, "Key F3.");

    return ret;
}

static t_libcli_status _libcli_key_home(void* data){

    t_libcli_status ret = LIBCLI_OK;

    t_libcli_ctx *ctx = (t_libcli_ctx *)data;
    //_libcli_putstr(ctx, "Key Home.");

    for(;ctx->cursor_pos > 0; ctx->cursor_pos--){
        _libcli_putstr(ctx, "\b \b");
    }
    ctx->input_buf[ctx->cursor_pos] = '\0';
   
    return ret;
}
static t_libcli_status _libcli_key_end(void* data){

    t_libcli_status ret = LIBCLI_OK;

    t_libcli_ctx *ctx = (t_libcli_ctx *)data;
    _libcli_putstr(ctx, "Key End.");

    return ret;
}
static t_libcli_status _libcli_key_pgup(void* data){

    t_libcli_status ret = LIBCLI_OK;

    t_libcli_ctx *ctx = (t_libcli_ctx *)data;
    _libcli_putstr(ctx, "Key Page Up.");

    return ret;
}
static t_libcli_status _libcli_key_pgdown(void* data){

    t_libcli_status ret = LIBCLI_OK;

    t_libcli_ctx *ctx = (t_libcli_ctx *)data;
    _libcli_putstr(ctx, "Key Page Down.");

    return ret;
}
static t_libcli_status _libcli_key_insert(void* data){

    t_libcli_status ret = LIBCLI_OK;

    t_libcli_ctx *ctx = (t_libcli_ctx *)data;
    _libcli_putstr(ctx, "Key Insert.");

    return ret;
}
static t_libcli_status _libcli_key_delete(void* data){

    t_libcli_status ret = LIBCLI_OK;

    t_libcli_ctx *ctx = (t_libcli_ctx *)data;
    //_libcli_putstr(ctx, "Key delete.");

    // Delete key does the same as backspace
    // TODO: Implement the required functionality to edit the command line
    if(ctx->cursor_pos){
        //ctx->input_buf[ctx->cursor_pos--] = '\0';
        ctx->input_buf[--(ctx->cursor_pos)] = '\0';
        _libcli_putstr(ctx, "\b \b");
    }


    return ret;
}


static t_libcli_status _libcli_key_aelig(void* data){

    t_libcli_status ret = LIBCLI_OK;

    t_libcli_ctx *ctx = (t_libcli_ctx *)data;
    _libcli_putstr(ctx, "æ");

    return ret;
}


static t_libcli_status _libcli_key_oslash(void* data){

    t_libcli_status ret = LIBCLI_OK;

    t_libcli_ctx *ctx = (t_libcli_ctx *)data;
    _libcli_putstr(ctx, "ø");

    return ret;
}


static t_libcli_status _libcli_key_aring(void* data){

    t_libcli_status ret = LIBCLI_OK;

    t_libcli_ctx *ctx = (t_libcli_ctx *)data;
    _libcli_putstr(ctx, "å");

    return ret;
}

static t_libcli_status _libcli_print_cmd_listing(t_libcli_ctx *ctx){

    t_libcli_status ret = LIBCLI_OK;
    uint8_t i = 0;

    _libcli_putstr(ctx, "'help' prints this command listing. 'lock' locks the terminal.\r\n");
    _libcli_putstr(ctx, "The following additional commands are supported:\r\n");

    while(ctx->cmd_table[i].cmd != NULL){
        _libcli_putstr(ctx, ctx->cmd_table[i].cmd);
        _libcli_putstr(ctx, "\r\n");
        i++;
    }

    _libcli_putstr(ctx, "\r\nPlease refer to the manual for details.\r\n");

    return ret;
}

t_libcli_status libcli_initial_prompt(t_libcli_ctx *ctx){

    t_libcli_status ret = LIBCLI_OK;

    _libcli_putstr(ctx, "\r\nVibe console initialized.\r\n");

    if(ctx->cli_locked){
        _libcli_auth_prompt(ctx, true);
    }
    else {
        _libcli_prompt(ctx, true);
    }

    return ret;
}

t_libcli_status libcli_read(t_libcli_ctx *ctx){

    t_libcli_status ret = LIBCLI_OK;

    char rx_byte = 0;

    if(ctx->getc(&rx_byte) == LIBCLI_OK){

        /** Look for multi byte key codes
         * For many of the keys, such as arrows, the first byte is always escape.
         * Here we look for the subsequent bytes.
         * 
         * TODO: Shift + Arrow causes letter. Figure out why.
        */
        if(ctx->mkey.pos && (ctx->mkey.pos < LIBCLI_MULTICODE_INPUT_MAX_BYTES)){
            uint8_t i = 0;

            ctx->mkey.input_buf[ctx->mkey.pos++] = rx_byte;

            // Loop through all available handlers
            while(ctx->mcmd[i].handler){

                if(!memcmp(ctx->mcmd[i].byte_pattern, ctx->mkey.input_buf, ctx->mcmd[i].len)){

                    ctx->mcmd[i].handler((void*)ctx);
                    ctx->mkey.pos = 0;
                    memset(ctx->mkey.input_buf, 0, sizeof ctx->mkey.input_buf / sizeof ctx->mkey.input_buf[0]);
                    //memset(ctx->mkey.input_buf, 0, LIBCLI_MULTICODE_INPUT_MAX_BYTES);
                    break;
                }
                i++;
            }
            return LIBCLI_OK;
        }

        switch (rx_byte)
        {
        case KEY_CODE_BACKSPACE:
            if(ctx->cursor_pos){
                ctx->input_buf[--(ctx->cursor_pos)] = '\0';
                _libcli_putstr(ctx, "\b \b");
            }
            break;

        /**
         * The Esc button sends a single byte 0x1B
         * Some buttons sends multiple bytes, where the first byte is 0x1B
        */
        case KEY_CODE_ESCAPE:
            ctx->mkey.input_buf[0] = rx_byte;
            ctx->mkey.pos = 1;
            break;
        //case KEY_CODE_ENTER:
        case KEY_CODE_CARRIAGE_RETURN:

            // Zero terminate the input buffer
            ctx->input_buf[ctx->cursor_pos < (LIBCLI_CMD_BUFFER_SIZE - 1) ? ctx->cursor_pos + 1 : ctx->cursor_pos] = '\0';

            _libcli_putstr(ctx, "\r\n");

            if(ctx->cli_locked){
                if(_libcli_authenticate(ctx) != LIBCLI_AUTH_OK){
                    _libcli_auth_prompt(ctx, false);
                }
                else{
                    _libcli_prompt(ctx, 1);
                }
            }
            else{
                ret = _libcli_interpret_cmd(ctx);
                _libcli_interpret_cmd_result(ctx, ret);

                if(ret == LIBCLI_AUTH_LOCKED){
                    _libcli_auth_prompt(ctx, false);
                }
                else{
                    _libcli_prompt(ctx, 1);
                }
            }
            ctx->cursor_pos = 0;
            memset(ctx->input_buf, 0x00, LIBCLI_CMD_BUFFER_SIZE);
            break;
        default:
            if((ctx->cursor_pos < (LIBCLI_CMD_BUFFER_SIZE - 1)) && isprint(rx_byte)){
                ctx->input_buf[ctx->cursor_pos++] = rx_byte;
                if(ctx->cli_locked){
                    char pwd_mask = '*';
                    ctx->putc(&pwd_mask);
                }
                else{
                    ctx->putc(&rx_byte);
                }
            }
            break;
        }

    }

    return ret;
}