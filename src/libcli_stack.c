#include "libcli_stack.h"


libcli_stack_res_t libcli_stack_init(libcli_stack_ctx_t *ctx){

    ctx->top = EMPTY;
    ctx->pos = 0;

    return RES_OK;
}

libcli_stack_res_t libcli_stack_push(libcli_stack_ctx_t *ctx, libcli_stack_data_t *data_in){

    if(ctx->top >= LIBCLI_STACK_LEN - 1){
        return RES_FULL;
    }

    ctx->stack[++ctx->top] = *data_in;
    ctx->pos = ctx->top;

    return RES_OK;
}

libcli_stack_res_t libcli_stack_pop(libcli_stack_ctx_t *ctx, libcli_stack_data_t *data_out){

    if(ctx->top == EMPTY){
        return RES_EMPTY;
    }

    if(data_out != NULL){
        *data_out = ctx->stack[ctx->top--];
        ctx->pos = ctx->top;
    }

    return RES_OK;
}


libcli_stack_res_t libcli_stack_traverse_down(libcli_stack_ctx_t *ctx, libcli_stack_data_t *data_out){

    if(ctx->top == EMPTY){
        return RES_EMPTY;
    }

    if(data_out != NULL){

        *data_out = ctx->stack[ctx->pos];
       
        if(ctx->pos > 0){
            ctx->pos--;
        }
        else{
            return RES_BOTTOM;
        }
    }

    return RES_OK;
}


libcli_stack_res_t libcli_stack_traverse_up(libcli_stack_ctx_t *ctx, libcli_stack_data_t *data_out){

    if(ctx->top == EMPTY){
        return RES_EMPTY;
    }


    
    if(data_out != NULL){

        *data_out = ctx->stack[ctx->pos];

        if(ctx->pos < ctx->top){
            ctx->pos++;
        }
        else{
            return RES_TOP;
        }
    }

    return RES_OK;
}