/* 
 * This file is part of vibe.
 * Copyright (c) 2021 Elesense AS
 * 
 * Author: Eirik Haustveit.
 * 
 * SPDX-License-Identifier: GPL-3.0-only
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CLI_COMMANDS_H
#define CLI_COMMANDS_H

//#ifdef __cplusplus
//extern "C" {
//#endif

void initialize_cli();

//#ifdef __cplusplus
//}
//#endif

#endif