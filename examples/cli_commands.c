
#include <stdlib.h>
//#include <unistd.h>
#include <getopt.h>

#include "libcli.h"
#include "logo.h"

/**
 * PA_9 is connected to USART1 TX. It is pin D9 on the Arduino connector.
 * PA_10 is connected to USART1 RX. It is pin A2 on the Arduino connector.
 * 
 * PB_6 is connected to USART1 TX. It is pin D1 on the Arduino connector.
 * PB_7 is connected to USART1 RX. It is pin D0 on the Arduino connector.
*/
#define MONITOR_TX_PIN                                                PB_6  //PA_9
#define MONITOR_RX_PIN                                                PB_7  //PA_10


/**
 * Create a buffered serial port for the monitor interface.
*/
static BufferedSerial monitor_port(MONITOR_TX_PIN, MONITOR_RX_PIN, 115200);

/**
 * Associate a file descriptor with the monitor port
 * in order to use fprintf() for printing on the UART.
 * 
 * Use w+ for both read and write access.
*/
FILE *mon = fdopen(&monitor_port, "w+");

static t_libcli_status print_version_func(int argc, char **argv);

static t_libcli_status print_test_func(int argc, char **argv);
static t_libcli_status print_hest_func(int argc, char **argv);
static t_libcli_status print_fest_func(int argc, char **argv);

static t_libcli_status argc_test_func(int argc, char **argv);
static t_libcli_status argv_test_func(int argc, char **argv);

static t_libcli_status argument_parsing_test_func(int argc, char **argv);

static t_libcli_cmd g_cmd_table[] = {
    {
        .cmd = "about",
        .fhandle = print_version_func,
        .cmd_flags = CMD_FLAG_NORMAL
    },
    {
        .cmd = "test",
        .fhandle = print_test_func,
        .cmd_flags = CMD_FLAG_NORMAL
    },
    {
        .cmd = "hest",
        .fhandle = print_hest_func,
        .cmd_flags = CMD_FLAG_NORMAL
    },
    {
        .cmd = "fest",
        .fhandle = print_fest_func,
        .cmd_flags = CMD_FLAG_NORMAL
    },
    {
        .cmd = "argc",
        .fhandle = argc_test_func,
        .cmd_flags = CMD_FLAG_NORMAL
    },
    {
        .cmd = "argv",
        .fhandle = argv_test_func,
        .cmd_flags = CMD_FLAG_NORMAL
    },
    {
        .cmd = "argparse",
        .fhandle = argument_parsing_test_func,
        .cmd_flags = CMD_FLAG_NORMAL
    },
	// null
	{ 0x00, 0x00, CMD_FLAG_NORMAL }
};

static t_libcli_status print_version_func(int argc, char **argv){
    t_libcli_status ret = LIBCLI_OK;

    //fprintf(mon, "\n\nVersion\n"); // Add version information here...

    return ret;
}

static t_libcli_status print_test_func(int argc, char **argv){

    t_libcli_status ret = LIBCLI_OK;
    
    //fprintf(mon, "Dette er ein test\n");

    return ret;
}

static t_libcli_status print_hest_func(int argc, char **argv){

    t_libcli_status ret = LIBCLI_OK;
    
    //fprintf(mon, hest);

    return ret;
}

static t_libcli_status print_fest_func(int argc, char **argv){

    t_libcli_status ret = LIBCLI_OK;
    
    //fprintf(mon, "Fest? Ja, eg er med!\n");
    //fprintf(mon, fest);

    return ret;
}

static t_libcli_status argc_test_func(int argc, char **argv){

    t_libcli_status ret = LIBCLI_OK;

    //fprintf(mon, "Argc: %i\n", argc);

    return ret;
}

static t_libcli_status argv_test_func(int argc, char **argv){

    t_libcli_status ret = LIBCLI_OK;

    //fprintf(mon, "Argv listing\n");

    //for(uint8_t i = 0; i < argc; i++){
    //    fprintf(mon, "Argv: \"%s\"\n", argv[i]);
    //}

    return ret;
}

static t_libcli_status argument_parsing_test_func(int argc, char **argv){

    t_libcli_status ret = LIBCLI_OK;

    //fprintf(mon, "Arg. parsing.\n");
    
    optind = 0;  // Reset the internal state of getopt()
    int opt = 0;
    while((opt = getopt(argc, argv, "f")) != -1){
        switch (opt)
        {
        case 'f':
            //fprintf(mon, "Option: %c\n", opt);
            break;
        
        default:
            break;
        }
    }

    return ret;
}

t_libcli_status uart_putc(const char *data_in){

    t_libcli_status ret = LIBCLI_OK;
    
    monitor_port.write(data_in,1);

    return ret;
}

t_libcli_status uart_getc(char *data_out){

    t_libcli_status ret = LIBCLI_OK;

    if(monitor_port.read(data_out, 1) < 0){
        ret = LIBCLI_E_IO;
    }

    return ret;
}

char my_cli_passwd[] = "1234";

void initialize_cli(){

    monitor_port.set_blocking(true);

    t_libcli_ctx ctx = {0};

    libcli_init(&ctx, g_cmd_table, uart_getc, uart_putc, true, my_cli_passwd);

    printf("Initialized CLI with size: 0x%zx\n", sizeof(ctx));
    
    while(1){
        libcli_read(&ctx);
    }
}
