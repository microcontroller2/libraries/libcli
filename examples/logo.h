#ifndef LOGO_H
#define LOGO_H

#ifdef __cplusplus
extern "C" {
#endif


const char *fest = "   _O/                   ,\n"
"     \\                  /           \\O_\n"
"     /\\_             `\\_\\        ,/\\/\n"
"     \\  `       ,        \\         /\n"
"     `       O/ /       /O\\        \\\n"
"             /\\|/\\.                `";


/**
 * A horse
*/
const char* hest = "                    "
   "             |\\    "
   "/|\r\n"
   "                    "
   "          ___| \\,,/"
   "_/\r\n"
   "                    "
   "       ---__/ \\/   "
   " \\\r\n"
   "                    "
   "      __--/     (D) "
   " \\\r\n"
   "                    "
   "      _ -/    (_    "
   "  \\\r\n"
   "                    "
   "     // /       \\_ "
   "/  -\\\r\n"
   "   __-------_____--_"
   "__--/           / \\"
   "_ O o)\r\n"
   "  /                 "
   "                /   "
   "\\__/\r\n"
   " /                  "
   "               /\r\n"
   "||          )       "
   "            \\_/\\\r"
   "\n"
   "||         /        "
   "      _      /  |\r"
   "\n"
   "| |      /--______  "
   "    ___\\    /\\  :"
   "\r\n"
   "| /   __-  - _/   --"
   "----    |  |   \\ \\"
   "\r\n"
   " |   -  -   /       "
   "         | |     \\ "
   ")\r\n"
   " |  |   -  |        "
   "         | )     | |"
   "\r\n"
   "  | |    | |        "
   "         | |    | |"
   "\r\n"
   "  | |    < |        "
   "         | |   |_/\r"
   "\n"
   "  < |    /__\\      "
   "          <  \\\r\n"
   "  /__\\             "
   "          /___\\\r\n"
   "\r\n";


#ifdef __cplusplus
}
#endif

#endif
